export const addPlaceholderToSearch = (inputSelector, placeholder = 'Search') => {
    const searchInput = document.querySelector(inputSelector);
    if ( searchInput) {
        searchInput.placeholder = placeholder;
    }
}
