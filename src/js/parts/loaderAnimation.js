import { gsap } from 'gsap';


/*
  Set some GSAP defaults
*/
gsap.defaults({
    duration: 2,
    ease: "power3.inOut"
});




/*
  fade in animation function
*/
export const loaderAnimation = () => {
    const loaderEl = document.querySelector('.c-loader');

    if (!loaderEl) {
        // console.log('loaderEl not found!');
        return false;
    }

    const logoTl = gsap.timeline({ paused: true });
    logoTl.delay(0.5);
    logoTl.to('#c-loader-logo-left, #c-loader-logo-bottom', { transformOrigin: 'bottom center', rotationX: -80});
    logoTl.to('#c-loader-logo', {scale: 2.8, duration: 2, ease: "power4.inOut"}, 0.4);
    // logoTl.to('#c-loader-logo', {opacity: 0, duration: 2, ease: "none"}, 0.8);
    logoTl.to('.c-loader', {autoAlpha: 0, duration: 1, ease: 'power4.out'}, 1.5);
    // logoTl.fromTo('.layout-container', {opacity: 0.8, y: '60px'}, {opacity: 1, y: 0, duration: 1, ease: "power4.out"}, 1.5);

    logoTl.restart();
}
