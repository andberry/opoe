import './parts/c-gallery.js';
import './parts/c-carousel.js';
import './parts/vimeo-in-lightbox.js';
import './parts/mobileMenuToggle.js';
import './parts/c-header-top-bar.js';
import { addPlaceholderToSearch } from './parts/search-improvements.js';
// import { loaderAnimation } from './parts/loaderAnimation';

import $ from "jquery";
import Foundation from 'foundation-sites';
$(".block-menu.menu--main > .menu")
    .addClass('vertical medium-horizontal menu')
    .data("responsive-menu", "accordion medium-dropdown");
const responsiveMenu = new Foundation.ResponsiveMenu(
    $(".block-menu.menu--main > .menu"),
    {}
);
// console.log(responsiveMenu);

addPlaceholderToSearch('#search-block-form .form-search');



/*  Loader animation */
/*
window.addEventListener('load', () => {
    loaderAnimation();
});
*/



// import './parts/scrollLinks.js';

/*
import './parts/slideshow.js';
import './parts/scroll-reveal-animations.js';

import { animateBackgroundZoomIn } from './parts/animations.js';
animateBackgroundZoomIn(document.querySelector('.has-anim-zoom-in'));
*/
