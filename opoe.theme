<?php
/*
    Add content-type to page templates suggestions,
    so we can use page--CONTENT-TYPE.html.twig
*/
function opoe_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $suggestions[] = 'page__' . $node->bundle();
  }
}



/*
    Field template suggestion
    for some fields we just need the field content, without wrapperd and labels:
    add fields names to $content_only_fields_single and $content_only_fields_multiple accordingly
*/
function opoe_theme_suggestions_field_alter(&$suggestions, &$variables) {
    $content_only_fields_single = array(
        // drupal node title aka label
        'title',

        // fields used across content-types / paragraphs
        'field_title',
        'field_text',
        'field_link',
        'field_image_position',
        'field_intro_text',
        'field_role',
        'field_footer_text',
        'field_text_left',
        'field_text_right',
        'field_add_branding',


        // image fields (useless here if we use field.0 ?)
        'field_image',

        // fields used in views: useless here, unless you use "Use field template" in field setting in view)
    );

    if (in_array($variables['element']['#field_name'], $content_only_fields_single)) {
        $suggestions[] = 'field__content_only__single';
    }


    $content_only_fields_multiple = array(
        'field_flexible_contents',
        'field_icon_and_content_blocks',
        'field_leaders',
        'field_cta',
        'field_newsletter_items'
    );

    if (in_array($variables['element']['#field_name'], $content_only_fields_multiple)) {
        $suggestions[] = 'field__content_only__multiple';
    }
}



function opoe_preprocess_html(&$variables) {
    /*
        Public or Professionals by default: add body class accordingly
    */
    $public_audience_class = 't-audience-public';
    $professionals_audience_class = 't-audience-professionals';

    $audience_select = theme_get_setting('opoe_audience_select');
    $public_audience_base_url = theme_get_setting('opoe_public_audience_baseurl');
    $professionals_audience_base_url = theme_get_setting('opoe_professionals_audience_baseurl');

    $current_drupal_path = \Drupal::service('path.current')->getPath();
    $current_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_drupal_path);

    if ($audience_select == 'public') {
        if ($professionals_audience_base_url && substr($current_alias, 0, strlen($professionals_audience_base_url)) == $professionals_audience_base_url) {
            $variables['attributes']['class'][] = $professionals_audience_class;
        } else {
            $variables['attributes']['class'][] = $public_audience_class;
        }
    } elseif ($audience_select == 'professionals') {
        if ($public_audience_base_url && substr($current_alias, 0, strlen($public_audience_base_url)) == $public_audience_base_url) {
            $variables['attributes']['class'][] = $public_audience_class;
        } else {
            $variables['attributes']['class'][] = $professionals_audience_class;
        }
    }



    /*
        Add path-alias as body class
    */
    $current_path = \Drupal::service('path.current')->getPath();
    $path = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
    if ($current_alias) {
        $current_alias = ltrim($current_alias, '/');
        $variables['attributes']['class'][] = 'path-' . \Drupal\Component\Utility\Html::cleanCssIdentifier($current_alias);
    }
}


/*
    Add theme setting: url alias to check for adding learn section class
*/
function opoe_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['opoe_audience_select'] = array(
    '#type'          => 'select',
    '#title'         => t('Public or Professionals by default?'),
    '#options'       => ['public' => 'Public', 'professionals' => 'Professionals'],
    '#default_value' => theme_get_setting('opoe_audience_select')
  );

  $form['opoe_public_audience_baseurl'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Public Audience Base URL'),
    '#default_value' => theme_get_setting('opoe_public_audience_baseurl'),
    '#description'   => t("Base url alias used for Public audience pages. Used only if Professionals by default.")
  );

  $form['opoe_professionals_audience_baseurl'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Professionals Audience Base URL'),
    '#default_value' => theme_get_setting('opoe_professionals_audience_baseurl'),
    '#description'   => t("Base url alias used for Professionals audience pages. Used only if Public by default.")
  );
}
